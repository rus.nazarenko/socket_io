//Server
const express = require('express')
const {Server} = require('socket.io')
const http = require('http')
const app = express()
// const cors = require('cors')
const server = http.createServer(app)
const io = new Server(server, {cors: {origin: true}})

// app.use(cors())

io.on('connection', (socket) => {
	console.log('Server: Client connected')

	socket.on('disconnect', (info) => {
		console.log('Server: User disconnected ===>>> ', info)
	})

  socket.on('send_message', (data, msg) => {
    console.log('Server: Send message ===>>> ', data, msg)
    socket.emit('reseive_message', data + ' - response from server')
  })
})

server.listen(3000, () => {
	console.log('Server: server is listening on port 3000')
})