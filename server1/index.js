//Client
const express = require('express')
const clientIo = require('socket.io-client')
const http = require('http')
const app = express()
// const cors = require('cors')
const server = http.createServer(app)
const client = clientIo("http://localhost:3000")
// const client = clientIo("https://4c22-146-120-160-102.ngrok.io")

// app.use(cors())

client.on("connect", () => {
  console.log("Client: Connect", client.id)
})

client.emit('send_message', 'Hello S-PRO academy', ' - request from the client')

client.on("disconnect", () => {
  console.log("Client: Disconnect", client.id)
})

client.on('reseive_message', (data) => {
  console.log("Client: Reseive message ===>>>", data)
})

server.listen(1337, () => {
	console.log('Client: Server is listening on port 1337')
})